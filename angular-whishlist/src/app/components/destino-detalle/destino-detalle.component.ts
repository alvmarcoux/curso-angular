import { Component, OnInit, InjectionToken, Inject, Injectable, forwardRef } from '@angular/core';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { AppState, APP_CONFIG, AppConfig } from 'src/app/app.module';
import { Store } from '@ngrx/store';
import { HttpClient } from '@angular/common/http';

class DestinosApiClientViejo {
  getById(id:String): DestinoViaje {
    console.log('Llamando por la clase vieja');
    return null;
  }
}

// interface AppConfig {
//   apiEndPoint: String; 
// }
const APP_CONFIG_VALUE: AppConfig= {
  apiEndPoint: 'mi_api.com' 
};

// const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient,
@Injectable()
class DestinosApiClientDecorated extends DestinosApiClient { 
  constructor(store: Store<AppState>, 
    @Inject(forwardRef(() => APP_CONFIG)) private configDec: AppConfig, 
    http: HttpClient){
    super(store, configDec, http);
  }
  getById(id:String):DestinoViaje {
    console.log('llamando por la clase decorada!');
    console.log('config: ' + this.configDec.apiEndPoint);
    return super.getById(id);
  }
}

// class DestinosApiClientDecorated extends DestinosApiClient{
  
// }


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  // template: `
  // <mgl-map
  //   [style]="style"
  //   [zoom]="[2]"
  // >
  // </mgl-map>
  // `,
  styleUrls: ['./destino-detalle.component.css'], 
  providers: [ 
    DestinosApiClient, 
    // { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }, 
    // { provide: DestinosApiClient, useClass: DestinosApiClientDecorated }, 
    // { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }     
  ], 
  styles: [`
  mgl-map {
    height: 50vh;
    width: 100vw;
  }
`]  
})

export class DestinoDetalleComponent implements OnInit {
  destino:DestinoViaje;
  style = {
    sources: {
      world: {
        type:'geojson', 
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    }, 
    version:8, 
    layers: [{
      'id': 'countries', 
      'type':'fill', 
      'source': 'world', 
      'layout': {}, 
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  }

  constructor(private route: ActivatedRoute, private destinoApiClient: DestinosApiClient) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    //console.log('this.destinoApiClient', this.destinoApiClient);
    console.log('id', id);
    //console.log('this.destinoApiClient.getById', this.destinoApiClient.getById(id));
    this.destino = this.destinoApiClient.getById(id);
  }

}
