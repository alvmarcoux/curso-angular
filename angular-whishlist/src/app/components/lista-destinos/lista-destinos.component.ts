import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})

export class ListaDestinosComponent implements OnInit {
    @Output() onItemAdded: EventEmitter<DestinoViaje>;
    @Output() onItemDeleted: EventEmitter<DestinoViaje>;
    // @Output() onClicked: EventEmitter<DestinoViaje>;
    updates:DestinoViaje[];
    all;
    trackingTagsCount;
    // destinos: DestinoViaje[];
    nombreListado: string;
    ulElements: string[];
    constructor(private _destinosApiClient: DestinosApiClient, private store:Store<AppState>) {
        this.onItemAdded = new EventEmitter();
        this.onItemDeleted = new EventEmitter();

        this.updates = [];
        this.store.select(state => state.destinos.favorito)
            .subscribe(d => {
                if (d != null) {
                    this.updates.push(d);//'Se ha elegido a ' + d.nombre)
                }
            });
        store.select(state => state.destinos.items).subscribe(items => this.all = items );
        this.store.select(state => state.destinos.trackingTags)
            .subscribe(t => {
                if (t != null) {
                    this.trackingTagsCount = t;
                }
            });
        
    }

    public get destinosApiClient(): DestinosApiClient { 
        return this._destinosApiClient
    }

    ngOnInit(): void {

    }

    agregado(d: DestinoViaje) {
        this.destinosApiClient.add(d);
        this.onItemAdded.emit(d);
        // this.store.dispatch(new NuevoDestinoAction(d));

        //this.destinos.push(d); //new DestinoViaje(d.nombre, d.url));
        //console.log(this.destinos);
        //return false;
    }

    elegido(d: DestinoViaje) {
        this.destinosApiClient.elegir(d);
        this.onItemDeleted.emit(d);
        // this.store.dispatch(new ElegidoFavoritoAction(d));
    }

    eliminado(d: DestinoViaje){
        this.destinosApiClient.elimina(d);
        // this.onItemAdded.emit(d);
    }

    reset(){
        this.destinosApiClient.reset();
        return false;
    }

    getAll() {

    }

}
