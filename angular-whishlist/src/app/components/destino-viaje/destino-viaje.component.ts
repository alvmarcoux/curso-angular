import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';
import {trigger, state, style, transition, animate } from '@angular/animations';
@Component({
    selector: 'app-destino-viaje',
    templateUrl: './destino-viaje.component.html',
    styleUrls: ['./destino-viaje.component.css'], 
    animations: [
        trigger('esFavorito', [
            state('estadoFavorito', style({
                backgroundColor:'PaleTurquoise',
                width:'100%'
            })), 
            state('estadoNoFavorito', style({
                backgroundColor: 'WhiteSmoke', 
                width:'90%'
            })),
            transition('estadoNoFavorito => estadoFavorito', [
                animate('2s')
            ]),
            transition('estadoFavorito => estadoNoFavorito', [
                animate('1s')
            ]),            
        ])
    ]
    // providers: [ 
    //     // { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }, 
    //     { provide: DestinosApiClient, useClass: DestinosApiClientDecorated }, 
    //     { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }     
    // ] 
})

export class DestinoViajeComponent implements OnInit {
    @Input() destino: DestinoViaje;
    @Input('idx') position: number;
    @HostBinding('attr.class') cssClass = 'col-md-4';
    @Output() onClicked: EventEmitter<DestinoViaje>;
    @Output() onDeleted: EventEmitter<DestinoViaje>;

    constructor(private store: Store<AppState>) {
        this.onClicked = new EventEmitter();
        this.onDeleted = new EventEmitter();
    }
    

    ngOnInit(): void {
    }

    ir() {
        this.onClicked.emit(this.destino);
        return false;
    }

    eliminar() {
        this.onDeleted.emit(this.destino);
        return false;
    }

    voteUp() {
        this.store.dispatch(new VoteUpAction(this.destino));
        return false;
    }

    voteDown() {
        this.store.dispatch(new VoteDownAction(this.destino));
        return false;
    }
}
