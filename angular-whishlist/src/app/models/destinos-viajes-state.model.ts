import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { DestinosApiClient } from './destinos-api-client.model';
import { HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

// ESTADO
export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
    trackingTags: number;
}

export function initializeDestinosViajesState() {
  return {
    items: [],
    loading: false,
    favorito: null, 
    trackingTags: 0
  };
}

// ACCIONES
export enum DestinosViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  ELIMINA_DESTINO = '[Destinos Viajes] Eliminado',
  VOTE_UP = '[Destinos Viajes] Vote Up',
  VOTE_DOWN = '[Destinos Viajes] Vote Down',
  VOTES_RESET = '[Destinos Viajes] Votes Reset',
  INIT_MY_DATA = '[Destinos Viajes] Init My Data',
  TRACKING_TAGS_UP = '[Destinos Viajes] Tracking tags Up'
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) {}
}

export class EliminaDestinoAction implements Action {
  type = DestinosViajesActionTypes.ELIMINA_DESTINO;
  constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
  type = DestinosViajesActionTypes.VOTE_UP;
  constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
  type = DestinosViajesActionTypes.VOTE_DOWN;
  constructor(public destino: DestinoViaje) {}
}

export class VotesResetAction implements Action {
  type = DestinosViajesActionTypes.VOTES_RESET;
  constructor() {}
}

export class InitMyDataAction implements Action {
  type = DestinosViajesActionTypes.INIT_MY_DATA;
  constructor(public destinos: string[]) {}
}

export class TrackingTagsAction implements Action {
  type = DestinosViajesActionTypes.TRACKING_TAGS_UP;
  constructor(public destino: DestinoViaje) {}
}

export type DestinosViajesActions = NuevoDestinoAction | EliminaDestinoAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction | VotesResetAction | TrackingTagsAction;

// REDUCERS
export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajesActions
): DestinosViajesState {
  switch (action.type) {
    case DestinosViajesActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).destinos;
      return {
          ...state,
          items: destinos.map((d) => new DestinoViaje(d, ''))
        };
    }
    case DestinosViajesActionTypes.NUEVO_DESTINO: {
      return {
          ...state,
          items: [...state.items, (action as NuevoDestinoAction).destino ]
        };
    }
    case DestinosViajesActionTypes.ELIMINA_DESTINO: {
      const index = state.items.indexOf((action as EliminaDestinoAction).destino);
      if (index > -1) {
        state.items.splice(index, 1);
      }
      console.log(state.items, 'elementos..');
      return {
          ...state
        };
    }
    case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
        state.items.forEach(x => x.setSelected(false));
        const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
        fav.setSelected(true);
        return {
          ...state,
          favorito: fav
        };
    }
    case DestinosViajesActionTypes.VOTE_UP: {
        const d: DestinoViaje = (action as VoteUpAction).destino;
        d.voteUp();
        return { ...state, 
        };
    }
    case DestinosViajesActionTypes.VOTE_DOWN: {
        const d: DestinoViaje = (action as VoteDownAction).destino;
        d.voteDown();
        return { ...state, 
        };
    }
    case DestinosViajesActionTypes.VOTES_RESET: {
      state.items.forEach(x => x.votesReset());
      return { ...state };
    }
    case DestinosViajesActionTypes.TRACKING_TAGS_UP: {
      const d: DestinoViaje = (action as TrackingTagsAction).destino;
      console.log('trackingTags', ++state.trackingTags);
      return { ...state, 
        // trackingTags:++state.trackingTags
      };
    }

  }
  return state;
}

// EFFECTS
@Injectable()
export class DestinosViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );
  @Effect()
  voteUpCambio$: Observable<Action> = this.actions$.pipe( 
    ofType(DestinosViajesActionTypes.VOTE_UP),
    map((action: VoteUpAction) => new TrackingTagsAction(action.destino)), 
  );
  @Effect()
  voteDownCambio$: Observable<Action> = this.actions$.pipe( 
    ofType(DestinosViajesActionTypes.VOTE_DOWN),
    map((action: VoteDownAction) => new TrackingTagsAction(action.destino)), 
  );  
  @Effect()
  elegidoCambio$: Observable<Action> = this.actions$.pipe( 
    ofType(DestinosViajesActionTypes.ELEGIDO_FAVORITO),
    map((action: ElegidoFavoritoAction) => new TrackingTagsAction(action.destino))
  );
  constructor(private actions$: Actions) {}
}