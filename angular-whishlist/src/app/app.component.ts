import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import {TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Destinos de viaje.';
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
  });
  
  constructor(private _translate:TranslateService){
    // console.log('***************** get translation');
    _translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    _translate.setDefaultLang('es');  
  }

  public get translate() : TranslateService {
    // console.log('this.trans..', this._translate);
    return this._translate;
  }
  
}

