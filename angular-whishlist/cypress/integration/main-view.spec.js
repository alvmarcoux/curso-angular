describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Destinos de viaje.');
        cy.get('h1 b').should('contain', 'HOLA es');
    })
    it('campo nombre inicia vacío', () => {
        cy.visit('http://localhost:4200');
        cy.get('#nombre').should('be.empty');
    })
    it('botón inicia invisible', () => {
        cy.visit('http://localhost:4200');
        cy.get('button[type=submit]').should('not.exist'); //.should('be.visible');
    })    
})  